from django.test import TestCase, Client,LiveServerTestCase
from django.urls import resolve
from .views import *

class UnitTest(TestCase):
    def setUp(self):
        super().setUp()
        self.user = User(username='Dani')
        self.user.set_password("TP1sayaKenaPlagiarism")
        self.user.save()

    def test_homepage_login(self):
        user = User.objects.create_user('Dana', 'fpradana@gmail.com', 'TP1sayaKenaPlagiarism')
        user.set_password('TP1sayaKenaPlagiarism')
        user.save()

        client = Client()
        response = client.get('')

        self.assertIn('login', response.content.decode('utf8'))
        
        logged_in = client.login(username='Dana', password='TP1sayaKenaPlagiarism')
        self.assertTrue(logged_in)

        response = client.get('')
        self.assertIn('Dana', response.content.decode('utf8'))

    def test_homepage_logout(self):
        user = User.objects.create_user('Dana', 'fpradana@gmail.com', 'TP1sayaKenaPlagiarism')
        user.set_password('TP1sayaKenaPlagiarism')
        user.save()

        client = Client()
        client.login(username='Dana', password='TP1sayaKenaPlagiarism')

        response = client.get('')
        self.assertIn('Dana', response.content.decode('utf8'))

        client.logout()

        response = client.get('')
        self.assertIn('login', response.content.decode('utf8'))

    


    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 404)

    def test_story_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
        
    def test_story_using_index_func2(self):
        found = resolve('/login')
        self.assertEqual(found.func, loginUser)
        
    def test_story_using_index_func3(self):
        found = resolve('/logout')
        self.assertEqual(found.func, logoutUser)
        
    def test_story_using_index_func4(self):
        found = resolve('/register')
        self.assertEqual(found.func, register)
        

    def test_story_using_landingpage_templates(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
        
    def test_haloApaKabar(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
        self.assertContains(response, 'Halo, Selamat Datang!')


# Create your tests here.
